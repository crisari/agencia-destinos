<?php

//////////////////////////////////////////////////// ATENCION! ///////////////////////////////////////////////////
// MANTENGA LA ESTRUCTURA INTACTA Y MODIFIQUE UNICAMENTE LOS TEXTOS QUE SE LE INDICAN Y SOLO ENTRE LAS COMILLAS //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Su nombre, o de su empresa (Sera el nombre del remitente y el que se refleja cuando le contacten)
$empresa = "www.visasegura.com";                               // Modifique unicamente los datos entre las comillas

// Su direccion de email
$su_email = "tramites@visasegura.com";            // Modifique unicamente los datos entre las comillas

// Firma para la autorespuesta
$firma = "
Devolveremos su mensaje a la mayor brevedad posible.
Gerencia
tramites@visasegura.com
";                                                         // Modifique unicamente los datos entre las comillas

?>