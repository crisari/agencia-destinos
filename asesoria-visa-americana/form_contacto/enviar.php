<?php

require_once("config.php");

function generateMessageID($prefix="40ftrq") {
$message_id = "<$prefix." 
. base_convert((double)microtime(), 10, 36) 
. "." . base_convert(time(), 10, 36) 
. "@" . $_SERVER['HTTP_HOST'] . ">";
return $message_id;
}

session_start();
if ($_POST['Action'] == "Check") {
if ($_SESSION['Codigo'] == $_POST['Codigo']) {

// respuesta automatica para el usuario
$nombre = $_POST['Nombre'];
$direccion = $_POST['Email'];
$asunto = $_POST['Asunto'];
$mensaje = $_POST['Mensaje'];

$texto_user ="Este email es una respuesta automatica para $nombre 

Le confirmamos que su mensaje ha sido recibido el ".date("d-m-Y - H:i")." y sera tratado a la mayor brevedad

====================
Copia de su mensaje:
====================

Mensaje recibido de $nombre, desde el email $direccion, en referencia a: $asunto

Texto de su mensaje: $mensaje

====================

$firma
";

$headers_user.= "Date: " . date("r") . "\n";
$headers_user.= "Message-ID: " . generateMessageID() . "\n";
$headers_user.= "MIME-Version: 1.0\r\n";
$headers_user.= "Content-type: text/plain;";
$headers_user.= "charset=iso-8859-1\r\n";
$headers_user.= "From: $empresa <$su_email>\n";
$headers_user.= "Reply-To: $empresa <$su_email>\n";
$headers_user.= "X-Sender: $empresa\n";
$headers_user.= "X-Mailer: $empresa\n";
$headers_user.= "X-Priority: 3\n";
$headers_user.= "Content-Transfer-Encoding: 8bit\n";

$subject_user = "Respuesta automatica: ($asunto)";

mail($direccion,$subject_user,$texto_user,$headers_user, "-f{$su_email}");
///////////////////////

// envio para el admin

$texto_admin = "Nuevo mensaje de $nombre recibido el ".date("d-m-Y - H:i")."

Texto del mensaje: $mensaje

$firma
";

$headers_admin.= "Date: " . date("r") . "\n";
$headers_admin.= "Message-ID: " . generateMessageID() . "\n";
$headers_admin.= "MIME-Version: 1.0\r\n";
$headers_admin.= "Content-type: text/plain;";
$headers_admin.= "charset=iso-8859-1\r\n";
$headers_admin.= "From: $empresa <$su_email>\n";
$headers_admin.= "Reply-To: $nombre<$direccion>\n";
$headers_admin.= "X-Sender: $empresa\n";
$headers_admin.= "X-Mailer: $empresa\n";
$headers_admin.= "X-Priority: 3\n";

$subject_admin = "Formulario: ($asunto)";

mail($su_email,$subject_admin,$texto_admin,$headers_admin, "-f{$su_email}");

$returnurl1 = "form_ok.php";
header("Location: ".$returnurl1);

} else {

$returnurl2 = "form_error.php";
header("Location: ".$returnurl2);

} exit; }

?>