	<a name="testimonios"></a>
	<h3 class="prev-indent-bot2">Comentarios y Testimonios</h3>

	<section class="testimonios">
		<article class="nombre">
			Andres Casas
		</article>
				<article class="fecha">
			Ibague, 05 Agosto de 2016
		</article>
		<article class="comentario">
			Gracias al buen servicio prestado me han aprobado la visa y viajare con mis hijos a Orlando Florida, quedo agradecidos con ustedes. Un saludo a todos. 
		</article>
		
	</section>


	<section class="testimonios">
		<article class="nombre">
			Luz Day Valbuena
		</article>
				<article class="fecha">
			Buga, 02 Agosto de 2016
		</article>
		<article class="comentario">
			El viaje a New York es un hecho, gracias a ustedes y a sus consejos me fue aprobada la visa americana, los tips, las posibles preguntas y el acompa�amiento fue de vital importancia, gracias se�ora Patricia por todo. 
		</article>
		
	</section>


	<section class="testimonios">
		<article class="nombre">
			Josefa Diaz
		</article>
				<article class="fecha">
			Barranquilla, 13 Julio de 2016
		</article>
		<article class="comentario">
			El acompa�amiento siempre se vio presente en el tiempo que duro el tramite, es evidente que ustedes trabajan con responsabiliad propia, quiero agradecer por prestarme tan excelente servicio.  
		</article>
		
	</section>


	<section class="testimonios">
		<article class="nombre">
			Antonio Avila
		</article>
				<article class="fecha">
			Cartagena, 05 Junio de 2016
		</article>
		<article class="comentario">
			Buenas noches, quiero agradecer la inmensa paciencia que tuvieron conmigo y con mi esposa, ya que estabamos muy nerviosos y confundidos. Al aclararnos todas las dudas se nos facilito 
			responder las preguntas del consul, es por esto que se nos ha confirmado que aprobamos para la visa americana, gracias por todo.  
		</article>
		
	</section>


	<section class="testimonios">
		<article class="nombre">
			Yuliana Morales
		</article>
				<article class="fecha">
			Pereira, 28 Mayo de 2016
		</article>
		<article class="comentario">
			Quedo infinitamente agradecida con ustedes por el buen servicio que me han brindado. De seguro los recomendare con mis amigos y familiares. 
		</article>
		
	</section>


	<section class="testimonios">
		<article class="nombre">
			Alexandra Torres
		</article>
				<article class="fecha">
			Cali, 05 Marzo de 2016
		</article>
		<article class="comentario">
			Buen dia, que tal? resulta que estaba llenisima de dudas respecto a la entrevista, me habian comentado que la asesoria con otras agencias no era muy completa
			pero la suya fue excelente ya que respondi tal cual usted me lo sugirio y me aprobaron la visa, MIL GRACIAS. 
		</article>
		
	</section>


	<section class="testimonios">
		<article class="nombre">
			Jose Ramirez
		</article>
				<article class="fecha">
			Bogota, 10 Febrero de 2016
		</article>
		<article class="comentario">
			Gracias a su buena asesoria me fue aprobada la visa americana, estoy muy entusiasmado por el viaje, por fin podre ver a mi hija, muchas gracias.
		</article>
		
	</section>


	<section class="testimonios">
		<article class="nombre">
			Diana Marcela
		</article>
				<article class="fecha">
			Bogota, 25 Enero de 2016
		</article>
		<article class="comentario">
			Buenas tardes, Se�ora Patricia estoy muy agradecida por la asesoria que me brindo a mi y a mi familia, ya que nos han aprobado la visa e iremos de viaje a disfrutar. Muchas gracias a Visa Segura.
		</article>
		
	</section>


	<section class="testimonios">
		<article class="nombre">
			Doris Beatriz 
		</article>
				<article class="fecha">
			Medellin, 10 Enero de 2016
		</article>
		<article class="comentario">
			�EXCELENTE SERVICIO, EXCELENTE ASESORIA! Gracias por toda la paciencia que nos has tenido, me voy muy contenta con la asesoria ya que resolviste todas mis dudas.
		</article>
		
	</section>


	<section class="testimonios">
		<article class="nombre">
			Sandra Martinez
		</article>
				<article class="fecha">
			Bogota, 20 Noviembre de 2014
		</article>
		<article class="comentario">
			Hola mi ni&ntilde;a, muchas gracias y te felicito por tu asesoria en este tramite para visa americana, ya que en realidad me informaste de todos los requisitos para la visa americana.
			Muchos exitos.
		</article>
		
	</section>
	
	
	<section class="testimonios">
		<article class="nombre">
			Liliana Aristizabal
		</article>
				<article class="fecha">
			Medellin, 7 de Noviembre de 2014
		</article>
		<article class="comentario">
			Hola Patricia, te quuiero contar que hoy me presente a la embajada americana y me aprobaron mi visa te doy gracias por la ayuda que me prestaste y estare en contacto para que me cotises los tiquetes a New York.
		</article>
		
	</section>
	
	<section class="testimonios">
		<article class="nombre">
			Maria Fernandez
		</article>
				<article class="fecha">
			Mexico DF, 3 de Noviembre de 2014
		</article>
		<article class="comentario">
			 Buenas noches Sr Patricia, Ya recibi la aprobacion y me sirvieron mucho los requisitos para la visa americana, que me aconsejaste, gracias por ayudarme a terminar el tramite.
		</article>
		
	</section>
	
	<section class="testimonios">
		<article class="nombre">
			Sandra Salazar
		</article>
				<article class="fecha">
			Quito, 21 de Octubre de 2014
		</article>
		<article class="comentario">
			 Buenas Noches Sra Lina,<br/><br/>
			Excelente asesoria para la visa americana se los recomiendo en verdad.
		</article>
		
	</section>
	
	<section class="testimonios">
		<article class="nombre">
			Susana Frick
		</article>
		<article class="fecha">
			Tunja; 05 de Octubre de 2014
		</article>
		<article class="comentario">
			Muchas gracias, todo salio tal cual usted me dijo, durante el tramite para la visa americana que realice esta semana, ya entre a su pagina y le deje los datos para mi viaje a las Cataratas del Niagara, espero me colabore con los tiquetes.
		</article>
		
	</section>
	
	<section class="testimonios">
		<article class="nombre">
			Ana Lucia Calderon
		</article>
		<article class="fecha">
			Cali, 15 de Septiembre de 2014
		</article>
		<article class="comentario">
			Buenas noches Patricia !!!!
			<br/><br/>
			Ya ma&ntilde;ana viajo para iniciar mis estudios en los Estados Unidos gracias por ayudarme con todos el tramite de visa americana para estudiante.
			
		</article>
		
	</section>
	
	<section class="testimonios">
		<article class="nombre">
			Esperanza Serna
		</article>
				<article class="fecha">
			Riohacha 14 de Septiembre de 2014
		</article>
		<article class="comentario">
			Doña Patricia
			<br/><br/>
			Ya nos llegaron los pasaportes con nuestra visa renovada muchas gracias, queria contarle que todos los requisitos para la visa americana que me aconsejo fueron los adecuados para presentar en el consulado.<br/>
			Muchas Gracias por su colaboracion
		</article>
		
	</section>
	
	<section class="testimonios">
		<article class="nombre">
			Obeida Suarez
		</article>
			<article class="fecha">
			Guayaquil, 02 de Septiembre de 2014
		</article>
		<article class="comentario">
			 Le Doy gracias por ayudarme a agendar mi cita, ya ma&ntilde;ana me presento en la embajada americana, espero todo me salga bien y la contactare para la compra de mis tiquetes como lo acordamos
		</article>
		
	</section>
	
	<section class="testimonios">
		<article class="nombre">
			Ana Maria Ubarne
		</article>
		<article class="fecha">
			Barranquilla, 24 de Agosto de 2014
		</article>
		<article class="comentario">
			Les doy gracias por el tramite de la visa americana y la asesoria que me prestaron, ya que anteriormente me habian negado la visa, pero con su ayuda ya me presente en la embaja y me aprobaron la visa americana.<br/>
			Bendiciones y muchos exitos
			<br/><br/>
			Mil Gracias.
		</article>
		
	</section>
	
	<section class="testimonios">
		<article class="nombre">
			Maritza Velasquez
		</article>
		<article class="fecha">
			Bogota, 19 de Agosto de 2014
		</article>
		<article class="comentario">
			Buenos dias!!
			<br/><br/>
			Te escribo para agradecerte de todo coraz&oacute;n, la amabilidad y colaboracion para el tramite de la visa familiar, le cuento que ya me presente ante la embajada americana y fue aprobada mi visa, ya toda mi familia esta lista para viajar a Orlando, Florida la puntualidad, no te hab&iacute;a escrito antes x que estaba de viaje.
			<br/><br/>
		</article>
		
	</section>
	
	