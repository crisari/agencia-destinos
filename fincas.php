<?php 
session_start ();

if (isset($_POST["lang"])) {
	$lang = $_POST["lang"];
	if(!empty($lang))
		{
		$_SESSION["lang"] = $lang;	
		}
}

if (isset($_SESSION["lang"])) 
{
	$lang= $_SESSION["lang"];
	require "lang/".$lang.".php";
} else {
	require "lang/es.php";
}

?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="<?php echo $lang ['descripcion'] ?>">
	<title><?php echo $lang ["paqiba"] ?></title>
	<link rel="stylesheet" href="">
	<link rel="stylesheet" href="1.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="css/icon.css">
	<link rel="shorcut icon" type="image/x-icon" href="imagenes/logo.png">
	<script type="text/javascript" src="jquery1.js"></script>
	<script src="javs/cojs.js" type="text/javascript" ></script>
<body id="inicio4">
   

<header class=cabecera>
		<div>
			<a href="inicio.php"><img  class="logo" src="iconos/encabezadologo.png" alt="logotipo"  ></a>
			<div class="icosoc">
				<ul class="redso">

					<li class="icored"><a target="_blank" href="https://www.facebook.com/destinos.tolima?fref=ts"><input type="image" name="redsoc" src="imagenes/facebook2.png" ></a></li>
					<li class="icored2"><input type="image" name="redsoc" src="imagenes/whatsapp.png"></li>
					<li class="icored2"><input type="image" name="redsoc" src="imagenes/gorjeo.png"></li>
				</ul>
			</div>
			<form class="uno" method="POST" >

				<input class="bot" name=lang type="image" src="imagenes/columbia.png" value="es">
				<input class="bot" name=lang type="image" src="imagenes/usa.png" value="en">
				<p class="idi">Idioma</p>
			</form>

		</div>
	</header>
	
	

		<ul class="menupag2" id="menupag3">
			<li data-go="have" class="dropdown"><a id="btn0"  href="inicio.php" data-action="open"><?php echo $lang['inicio'] ?><i class="fa fa-arrow-circle-right" id="hoche" style="margin-left:10px"></a></i>

					<ul class="desplega">
							<li><a href="paqueteinternacional.php"><?php echo $lang['paqint'] ?></a></li>
							<li><a href="paquetes.php"><?php echo $lang['paqnac'] ?></a></li>
							<li><a href=""><?php echo $lang['visame'] ?></a></li>
							<li><a href=""><?php echo $lang['alqfin'] ?></a></li>
							<li><a href=""><?php echo $lang['vuehot'] ?></a></li>
						</ul>

				</li class=>
			<a id="btn2"  data-insert="paque1" href="#"  data-ac="op"><li>Refugio de la Montaña</li></a>
			<a id="btn2"  data-insert="paque2" href="#"  data-ac="op"><li>Villa del Cantar</li></a>
			<a id="btn2"  data-insert="paque3" href="#"  data-ac="op"><li>Villa Carolina</li></a>
			<a id="btn2" data-insert="paque4" href="#" data-ac="op"><li>Villa Cristina</li></a>
			<a id="btn2"  data-insert="paque3" href="#"  data-ac="op"><li>Villa Esueño</li></a>
			<a id="btn2" data-insert="paque4" href="#" data-ac="op"><li>Villa Leones</li></a>			
		</ul>
	
	
	 		<div class="conpaquenac" id="paque1" style="display:none">
				<div class="n1ac1" id="1pa">	
				<h2 class="n1ac1"><?php echo $lang['cajamarca'] ?></h2>
				<p class="n1ac1"><?php	echo $lang['paquetecajamarca']; ?></p>
				<img src="fincas/refmont.jpg" alt="">
				<img src="fincas/refmont.jpg" alt="">		
				</div>
				
			</div>	
			<div class="conpaquenac" id="paque2" style="display:none">
				<div class="n1ac1" id="1pa">		
					<h2 class="n1ac1"><?php echo $lang['extremo'] ?></h2>
					<p class="n1ac1"><?php echo $lang['paqueteextremo'] ?></p>
					<img src="fincas/villcan.jpg" alt="">
					<img src="fincas/villcan.jpg" alt="">			
				</div>			
						
			</div>	
			<div class="conpaquenac" id="paque3" style="display:none">
				<div class="n1ac1" id="1pa">		
					<h2 class="n1ac1"><?php echo $lang['ibaguemusical'] ?></h2>
					<p class="n1ac1"><?php echo $lang['paqueteibague'] ?></p>
					<img src="fincas/villcar.jpg" alt="">
					<img src="fincas/villcar.jpg" alt="">			
				</div>			
						
			</div>	
			<div class="conpaquenac" id="paque4" style="display:none">
				<div class="n1ac1" id="1pa">		
					<h2 class="n1ac1"><?php echo $lang['ibaguenatural'] ?></h2>
					<p class="n1ac1"><?php echo $lang['paqueteibaguenatural'] ?></p>
					<img src="fincas/villcris.jpg" alt="">
					<img src="fincas/villcris.jpg" alt="">			
				</div>			
							
			</div>	
		
	</div>		
	<div class="dere" id="polsos" style="display:none">
		<div class="poli">
		<h3><?php echo $lang['polisos'] ?></h3>
		<p class="politica"><?php echo $lang['politica'] ?>			
		</p>
		</div>
	</div>
	<div class="dere" id="polsos1" style="display:none">
		<div class="poli">
		<h3 class="vi"><?php echo $lang['tituvision'] ?></h3>
		<p class="politica"><?php echo $lang['vision'] ?></p>
		<h3 class="vi"><?php echo $lang['titumision'] ?></h3>
		<p class="politica"><?php echo $lang['mision'] ?></p>

		</div>
	</div>
	<div class="dere" id="polsos2" style="display:none">
		<ul class="contactanos">
			<li><a href="https://www.facebook.com/destinos.tolima?fref=ts" title=""><input class="c" type="image" src="imagenes/facebook.png"><p class="c">Destinos Tolima</p></a></li>
			<li><input class="c" type="image" src="imagenes/tecnologia.png"><p class="c">0573204199366</p></li>
			<li><input class="c" type="image" src="imagenes/gmail-1.png"><p class="c"></p></li>
			<li><a  href=""><input class="c" type="image" src="imagenes/whatsapp-1.png"><p class="c">+0573204199366</p></a></li>
		</ul>
	</div>
	<footer id="pie">
			<img class="icologo" id="avion" data-grado="one" src="imagenes/titulo2.png" alt="logotipo" >
		<div class="pies">
			<h3 id="btn1" data-conima="polsos" href="#"  data-action="open" class="titu0"><?php echo $lang['polisos'] ?></h3>
			
			<h3 id="btn1" data-conima="polsos1" href="#"  data-action="open" class="titu1" ><?php echo $lang['nosotros'] ?></h3>
			<h3 id="btn1" data-conima="polsos2" href="#"  data-action="open" class="titu2"><?php echo $lang['contac'] ?></h3>
			
		</div>
	</footer>

	

</body>
</html>