<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <title>Contact us</title>
   
		<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
		<![endif]-->
  </head>
  <body>
    
    <div class="page">
      <?php require __DIR__.'/haeder.php'; ?>

      <section class="section-30 section-md-40 section-lg-66 section-xl-bottom-90 bg-gray-dark page-title-wrap" style="background-image: url(<?php echo $base_url ?>asesorias/images/imagenes/6.jpeg);">
        <div class="container">
          <div class="page-title">
            <h2>Contactenos</h2>
          </div>
        </div>
      </section>

      <section class="section-60 section-md-top-90 section-md-bottom-100">
        <div class="container">
          <div class="row row-50 justify-content-lg-between">
            <div class="col-lg-5 col-xl-4">
              <div class="inset-lg-right-15 inset-xl-right-0">
                <div class="row row-30 row-md-40">
                  <div class="col-md-10 col-lg-12">
                    <h3>Como Contactarnos</h3>
                    <p class="text-secondary">
                      Si tiene preguntas, puede llamarnos o escribirnos al whatsapp, nuestros asesores estaran gustosos de atenderlos.
                      
                    </p>
                  </div>
                  <div class="col-md-6 col-lg-12">
                    <h5>Oficina 1</h5>
                    <address class="contact-info">
                      <p class="text-uppercase">Ibague, COlombia.</p>
                      <dl class="list-terms-inline">
                        <dt>celular</dt>
                        <dd><a class="link-secondary" href="tel:#">+57 320 419 9366</a></dd>
                      </dl>
                      <dl class="list-terms-inline">
                        <dt>Email</dt>
                        <dd><a class="link-primary" href="mailto:#">ventas@agenciadestinos.net</a></dd>
                      </dl>
                    </address>
                  </div>
                  <div class="col-md-6 col-lg-12">
                    <h5>Oficina  2</h5>
                    <address class="contact-info">
                      <p class="text-uppercase">Bogota</p>
                      <dl class="list-terms-inline">
                        <dt>celular</dt>
                        <dd><a class="link-secondary" href="tel:#">+57 321 3232524</a></dd>
                      </dl>
                      <dl class="list-terms-inline">
                        <dt>E-mail</dt>
                        <dd><a class="link-primary" href="mailto:#">asesoria@apruebatuvisa.com</a></dd>
                      </dl>
                    </address>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-7 col-xl-6">
              <h3>Envianos un mensaje</h3>
              <form class="rd-mailform form-modern" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php">
                <div class="row row-30">
                  <div class="col-md-6">
                    <div class="form-wrap">
                      <input class="form-input" id="contact-name" type="text" name="name" data-constraints="@Required">
                      <label class="form-label" for="contact-name">Nombre</label>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-wrap">
                      <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
                      <label class="form-label" for="contact-email">Email</label>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-wrap">
                      <div class="textarea-lined-wrap">
                        <textarea class="form-input" id="contact-message" name="message" data-constraints="@Required"></textarea>
                        <label class="form-label" for="contact-message">Mensaje</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="row row-30 row-sm-0">
                      <div class="col-sm-8">
                        <button class="button button-primary button-block" type="submit">Enviar</button>
                      </div>
                      <div class="col-sm-4">
                        <button class="button button-silver-outline button-block" type="reset">Borrar</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>

      <!-- <section>
        <div class="google-map-container" data-zoom="5" data-center="9870 St Vincent Place, Glasgow, DC 45 Fr 45." data-styles="[{&quot;featureType&quot;:&quot;administrative&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;on&quot;},{&quot;lightness&quot;:33}]},{&quot;featureType&quot;:&quot;landscape&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#f2e5d4&quot;}]},{&quot;featureType&quot;:&quot;poi.park&quot;,&quot;elementType&quot;:&quot;geometry&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#c5dac6&quot;}]},{&quot;featureType&quot;:&quot;poi.park&quot;,&quot;elementType&quot;:&quot;labels&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;on&quot;},{&quot;lightness&quot;:20}]},{&quot;featureType&quot;:&quot;road&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;lightness&quot;:20}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;elementType&quot;:&quot;geometry&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#c5c6c6&quot;}]},{&quot;featureType&quot;:&quot;road.arterial&quot;,&quot;elementType&quot;:&quot;geometry&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#e4d7c6&quot;}]},{&quot;featureType&quot;:&quot;road.local&quot;,&quot;elementType&quot;:&quot;geometry&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#fbfaf7&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;on&quot;},{&quot;color&quot;:&quot;#acbcc9&quot;}]}]" data-icon="images/gmap_marker.png" data-icon-active="images/gmap_marker_active.png">
          <div class="google-map"></div>
          <ul class="google-map-markers">
            <li data-location="9870 St Vincent Place, Glasgow, DC 45 Fr 45." data-description="9870 St Vincent Place, Glasgow"></li>
          </ul>
        </div>
      </section> -->
<?php require __DIR__.'/footer.php'; ?>

    </div>
    <div class="snackbars" id="form-output-global"></div>
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>