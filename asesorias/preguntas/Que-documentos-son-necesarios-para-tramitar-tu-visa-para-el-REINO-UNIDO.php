<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <title>Que documentos son necesarios para tramitar tu visa para el REINO UNIDO</title>
    
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="<?php echo $base_url ?>asesorias/images/imagenes/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <div class="page">
     <?php require __DIR__.'/../haeder.php'; ?>

<section class="section-40 section-md-bottom-100 section-xl-bottom-165">
        <div class="container">
          <h3>Que documentos son necesarios para tramitar tu visa para el REINO UNIDO</h3>
          <div class="row row-30 justify-content-lg-between">
            <div class="col-md-6">
              <figure><img src="<?php echo $base_url ?>asesorias/images/imagenes/2-uk.jpeg" alt="" width="570" height="386"/>
              </figure>
            </div>
            <div class="col-md-6">
              <div class="inset-lg-left-40 inset-xl-left-70 text-secondary">
               
              
             <li> Pasaporte electrónico o de lectura mecánica este debe de tener  mínimo una vigencia de seis meses. </li>
            <li> Documentos que demuestren que tiene los medios  económicos para los gastos de  su viaje durante su estadía allí, que son los tiquetes aéreos, alimentación, hotel si no tiene carta de invitación de familiar. Se debe adjuntar extractos bancarios de los últimos dos trimestres con un saldo suficiente según el tiempo que este allí. También CDTS, certificación bancaria y similares, estos pueden ser del anfitrión en el Reino Unido o de la persona que aplica.</li>
            <li> Certificado o contrato que corrobore su estado civil.</li>
            <li> Certificación laboral o certificado que demuestre su ocupación, bien sea estudiante empleado o independiente</li>
            <li> Carta de responsabilidad de gastos de la persona que paga los gastos del viaje</li>
            <li> Para menores de edad: Registro civil de nacimiento, y documentos de los padres para comprobar parentesco. Y de igual manera datos de la persona con la que viaja y autorización si o viaja con sus padres o tutor legal </li>
            <li> Si usted visita familiares o amigos en el Reino Unido debe de adjuntar carta de invitación y documentos que demuestren la permanencia legal de la persona en el país, con certificación del lugar donde vive o carta de arrendatario.</li>
            <li> Si su visa es de trabajo debe de traer carta de invitación de la empresa con número de autorización del estado que será corroborado automáticamente en línea durante el trámite. </li>
            <li> reservas aéreas y hoteleras</li>
            <li> Tener en cuenta</li>
 <p>Todos los documentos deben de estar en español y traducidos a ingles por traductor certificado. La Agencia Destinos puede brindar este servicio por un costo adicional.</p>


              </div>
            </div>
         <h4>Preguntas</h4>
            <?php require __DIR__.'/visa-reino-unido-preguntas.php'; ?>
          </div>
        </div>
</section>

<?php require __DIR__.'/../footer.php'; ?>

    </div>
    <div class="snackbars" id="form-output-global"></div>
    <script src="<?php echo $base_url ?>asesorias/js/core.min.js"></script>
    <script src="<?php echo $base_url ?>asesorias/js/script.js"></script>
  </body>
</html>