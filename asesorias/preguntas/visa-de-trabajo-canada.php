<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <title>¿Que necesito para la Visa para trabajo?</title>
    
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="<?php echo $base_url ?>asesorias/images/imagenes/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <div class="page">
     <?php require __DIR__.'/../haeder.php'; ?>


<section class="section-40 section-md-bottom-100 section-xl-bottom-165">
        <div class="container">
          <h3>¿Que necesito para la Visa para trabajo?</h3>
          <div class="row row-30 justify-content-lg-between">
            <div class="col-md-6">
              <figure><img src="<?php echo $base_url ?>asesorias/images/imagenes/4-canada.jpeg" alt="" width="570" height="386"/>
              </figure>
            </div>
            <div class="col-md-6">
              <div class="inset-lg-left-40 inset-xl-left-70 text-secondary">
                <p>Tenga en cuenta que el gobierno de Canadá cuenta con políticas migratorias frente a la llegada de  turistas y personas que desean laboral allí. Debido a esto existen requisitos indispensables para la  aprobación de su visa, algunos documentos pueden ser:</p>
                <ul>
                <li>	Formulario debidamente diligenciado</li>
				<li>	Su currículo lo más actualizado posible.</li>
				<li>	Perfil educativo.</li>
				<li>	Dominio de inglés o francés.</li>
				<li>	Información sobre su familia inmediata. (padres hermanos e hijos)</li>
				<li>	Prueba de que se cumple con los requisitos del trabajo ofrecido</li>
				<li>	Cartas de referencia de empleo</li>
				<li>	Requisitos del empleo para el que aplica</li>


                </ul>
               
              </div>
            </div>
          <h4>Preguntas</h4>
            <?php require __DIR__.'/visa-canada.php'; ?>
          </div>
        </div>
</section>

<?php require __DIR__.'/../footer.php'; ?>

    </div>
    <div class="snackbars" id="form-output-global"></div>
    <script src="<?php echo $base_url ?>asesorias/js/core.min.js"></script>
    <script src="<?php echo $base_url ?>asesorias/js/script.js"></script>
  </body>
</html>