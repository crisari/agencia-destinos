<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <title>Que es la  Visa trabajo nivel 1(Inversionista, emprendedor, empresario) Reino Unido</title>
    
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="<?php echo $base_url ?>asesorias/images/imagenes/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <div class="page">
     <?php require __DIR__.'/../haeder.php'; ?>
<section class="section-40 section-md-bottom-100 section-xl-bottom-165">
        <div class="container">
          <h3> Que es la  Visa trabajo nivel 1(Inversionista, emprendedor, empresario) Reino Unido</h3>
          <div class="row row-30 justify-content-lg-between">
            <div class="col-md-6">
              <figure><img src="<?php echo $base_url ?>asesorias/images/imagenes/2-uk.jpeg" alt="" width="570" height="386"/>
              </figure>
            </div>
            <div class="col-md-6">
              <div class="inset-lg-left-40 inset-xl-left-70 text-secondary">
               <li><strong>Emprendedor: </strong></li>
              <p>Puede solicitar una visa de Nivel 1 (Emprendedor) si: quieres configurar o administrar una empresa en el Reino Unido; eres de fuera del Espacio Económico Europeo (EEE) y Suiza; cumple con los otros requisitos de elegibilidad; Debe tener acceso a fondos de inversión de al menos a 50.000 euros para solicitar esta visa</p>
             <li> <strong>Cuánto cuesta: </strong>1228 Euros </li>
            <li> <strong>Cuánto tiempo puede permanecer: </strong> <br> Puede venir al Reino Unido con una visa Tier 1 (Emprendedor) por un máximo de 3 años y 4 meses. Puede solicitar la extensión de esta visa por otros 2 años si ya se encuentra en esta categoría y 3 años si la cambia de otra categoría. Puede solicitar la conciliación (lo que se conoce como "licencia indefinida para quedarse") una vez que haya estado en el Reino Unido durante 5 años.</li>
            <li><strong>Que puede hacer: </strong>Configurar o hacerse cargo de un negocio o más; trabaje para su negocio, incluido el trabajo por cuenta propia, pero debe verificar que su trabajo cumpla con las condiciones de ser trabajador por cuenta propia; traer miembros de la familia contigo.</li>
            <li> <strong>No puedes: </strong><br>Hacer cualquier trabajo fuera de su negocio, por ejemplo, trabajo en el que está empleado por otra empresa; obtener fondos públicos
            </li>

            <br><br>
            <li><strong>Inversionista: </strong></li>
            <p>Puede solicitar una visa de Nivel 1 (Inversionista) si: quieres invertir £ 2,000,000 o más en el Reino Unido; eres de fuera del Espacio Económico Europeo (EEE) y Suiza; cumple con los otros requisitos de elegibilidad.</p>
            <li> <strong>Cuánto cuesta: </strong>1561 Euros </li>
            <li> <strong>Cuánto tiempo puede permanecer: </strong>Puede viajar al Reino Unido con una visa Tier 1 (Investor) por un máximo de 3 años y 4 meses. Puede solicitar extender esta visa por otros 2 años. </li>
            <li> <strong>Que puede hacer?: </strong>Invertir £ 2,000,000 o más en bonos del gobierno del Reino Unido, capital social o capital de préstamo en compañías registradas activas y en el Reino Unido, trabajar o estudiar, aplicar para liquidar después de 2 años si inviertes £ 10 millones, aplicar para liquidar después de 3 años si invierte £ 5 millones. </li>
            <li> <strong>No puedes: </strong>invertir en empresas dedicadas principalmente a inversiones inmobiliarias, administración de propiedades o desarrollo inmobiliario; trabajar como deportista profesional o entrenador deportivo; obtener fondos públicos; Tampoco puede trabajar como médico o dentista en capacitación a menos que se aplique uno de los siguientes: tiene un título primario a nivel de licenciatura o superior en medicina u odontología de una institución del Reino Unido que posee una licencia de patrocinador de nivel 4 o es un organismo reconocido o incluido en la lista del Reino Unido, trabajó como médico o dentista en capacitación la última vez que estuvo en el Reino Unido, ninguna de esas condiciones formaba parte de los términos y condiciones de una visa anterior </li>
              </div>
            </div>
            <h4>Preguntas</h4>
            <?php require __DIR__.'/visa-reino-unido-preguntas.php'; ?>
          </div>
        </div>
</section>

<?php require __DIR__.'/../footer.php'; ?>

    </div>
    <div class="snackbars" id="form-output-global"></div>
    <script src="<?php echo $base_url ?>asesorias/js/core.min.js"></script>
    <script src="<?php echo $base_url ?>asesorias/js/script.js"></script>
  </body>
</html>