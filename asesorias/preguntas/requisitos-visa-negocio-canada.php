<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <title>¿Cuáles son los Requisitos para la visa de estudio?</title>
    
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="<?php echo $base_url ?>asesorias/images/imagenes/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <div class="page">
     <?php require __DIR__.'/../haeder.php'; ?>

<section class="section-40 section-md-bottom-100 section-xl-bottom-165">
        <div class="container">
          <h3>¿Cuáles son algunos de los requisitos para tramitar la visa de negocios en Canadá?</h3>
          <div class="row row-30 justify-content-lg-between">
            <div class="col-md-6">
              <figure><img src="<?php echo $base_url ?>asesorias/images/imagenes/3-canada.jpeg" alt="" width="570" height="386"/>
              </figure>
            </div>
            <div class="col-md-6">
              <div class="inset-lg-left-40 inset-xl-left-70 text-secondary">
                <p>Si su deseo es hacer un emprendimiento o negociar con inversionistas canadienses este tipo de visado está acorde a su necesidad, tenga en cuenta algunos requisitos:</p>
                <ul>
                	<li>  Tarjeta de crédito o débito, o cuenta de PayPal en su caso.</li>
                 <li>  Pasaporte vigente</li>
                 <li> Documentos probatorios académicos. </li>
                 <li> Y demás documentos que demuestren sus negocios, su solvencia económica y experiencia en la actividad que desea desarrollar allí</li>

                </ul>
               
              </div>
            </div>
          <h4>Preguntas</h4>
            <?php require __DIR__.'/visa-canada.php'; ?>
          </div>
        </div>
</section>

<?php require __DIR__.'/../footer.php'; ?>

    </div>
    <div class="snackbars" id="form-output-global"></div>
    <script src="<?php echo $base_url ?>asesorias/js/core.min.js"></script>
    <script src="<?php echo $base_url ?>asesorias/js/script.js"></script>
  </body>
</html>
       