<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <title>Que es la Visa de estudiante de corta Duración</title>
    
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="<?php echo $base_url ?>asesorias/images/imagenes/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <div class="page">
     <?php require __DIR__.'/../haeder.php'; ?>

<section class="section-40 section-md-bottom-100 section-xl-bottom-165">
        <div class="container">
          <h3>Que es la Visa de estudiante de corta Duración</h3>
          <div class="row row-30 justify-content-lg-between">
            <div class="col-md-6">
              <figure><img src="<?php echo $base_url ?>asesorias/images/imagenes/5-uk.jpg" alt="" width="570" height="386"/>
              </figure>
            </div>
            <div class="col-md-6">
              <div class="inset-lg-left-40 inset-xl-left-70 text-secondary">
               
              <p>Visa de estudio de corto plazo o corta duración
              Si viaja al Reino unido para un curso corto Puede solicitar una visa de estudio a corto plazo; Si eres de fuera del Espacio Económico Europeo (EEE) y Suiza</p>
             <li> <strong>Cuánto cuesta el permiso de estudio: </strong><br>93 Euros por una visa de 6 meses <br>179 Euros por una visa de 11 meses </li>
            <li> <strong>Cuanto tiempo puedo permanecer: </strong> <br> Puede permanecer de 6 a 11 meses depende el tipo de curso de estudio que vaya a realizar </li>
            <li><strong>Lo que puedes hacer: </strong>Hacer un curso corto de estudio en el Reino Unido, como un curso de inglés o un curso de capacitación; haga un breve período de investigación como parte de un curso si está estudiando en el extranjero</li>
            <li> <strong>Lo que no puedes hacer con esta visa: </strong><br>Estudiar en una escuela financiada por el estado; trabajo (incluso en una colocación de trabajo o experiencia laboral) o llevar a cabo cualquier negocio; extender esta visa; traer a los miembros de la familia ('dependientes') con usted - deben presentar la solicitud por separado; obtener fondos públicos
            </li>
            


              </div>
            </div>
           <h4>Preguntas</h4>
            <?php require __DIR__.'/visa-reino-unido-preguntas.php'; ?>
          </div>
        </div>
</section>

<?php require __DIR__.'/../footer.php'; ?>

    </div>
    <div class="snackbars" id="form-output-global"></div>
    <script src="<?php echo $base_url ?>asesorias/js/core.min.js"></script>
    <script src="<?php echo $base_url ?>asesorias/js/script.js"></script>
  </body>
</html>