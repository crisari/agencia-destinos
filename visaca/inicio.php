
 	
<!DOCTYPE html>
<html>
<head>
  <?php $base_url = "https://agenciadestinos.net/" ?>
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" type="text/css" href="formato.css">
	<script type="text/javascript" src="jquery1.js"></script>
	<script  type="text/javascript" src="formato.js" ></script>
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
	


</head>
<body >
<div id="particles-js">
	
</div>
<div class="contresp">
		<header id="header" class="">
			<a href="../index.php "><img class="logo"  src="imagenes/logoblancoynegro.png" alt="logotipo"  ></a>
			
			<div id="menubar">
				<a href="#" class="bt-menu"><p>Menu</p> <img src="imagenes/menu1.png" alt=""></a>

				
			</div>
				<nav>
					<ul id="menu">
						<li id="limenu" data-abrir="uno"  data-action="close">Paquetes de viajes
						<ul id="submenu" class="uno">
							<a href="https://agenciadestinos.net/asesorias/visados.php"><li>Nacionales</li></a>
							<a href="https://agenciadestinos.net/asesorias/visados.php"><li>Locales</li></a>
							<a href="https://agenciadestinos.net/asesorias/visados.php"><li>Internacionales</li></a>

						</ul></li>
						
						

						<li id="limenu" data-abrir="cuatro" data-action="close">Servicios 
						<ul id="submenu" class="cuatro">
							<a href="https://agenciadestinos.net/asesorias/visados.php"><li>Visa americana</li></a>
							<a href="https://agenciadestinos.net/asesorias/visados.php"><li>Vuelos y hoteles</li></a>
							<a href="https://agenciadestinos.net/asesorias/visados.php"><li>Alquiler de fincas</li></a>
							<a href="https://agenciadestinos.net/asesorias/visados.php"><li>Seguros de viajes</li></a>

						</ul></li>

					</ul>
				</nav>		
		</header>

		<section id="seccion1">
			<img class="imagen1" src="imagenes/banderacanada2.png" alt="">
			

		</section>

		<section id="seccionslider">
			<div id="slidermove">
				<section id="li" data><img id="ima" src="imagenes/sli1.jpg" alt=""></section>
				<section id="li" data><img id="ima" src="imagenes/sli2.jpg" alt=""></section>
				<section id="li" ><img id="ima" src="imagenes/sli3.jpg" alt=""></section>
			</div>
			<div>
					<div id="btn-prev">&#60;</div>
					<div id="btn-next">&#62;</div>
			</div>
			


			
			
		</section>
		<section id="seccion2">
			<div id="conprim" class="primuno">
				<div id="conimage"><img id="iconos" src="imagenes/inter2.png" alt="">
				</div>
				<div id="context">
				<h2>Visa Turista</h2>
				<p> Te asesoramos a escoger que tipo de visa te es elegible para viajar a canada.
				asesoria profesional en todo el proceso y servicio personalizado.
				diligenciamiento de formularios de manera completa y eficas para incremetar posibilidades.
				 </p>
				</div>
			</div>
			<div id="conprim">
				<div id="conimage"><img id="iconos" src="imagenes/verificar.png" alt="">
				</div>
				<div id="context">
				<h2>Requisitos</h2>
				<p>los requisitos para iniciar el tramite.
				-debe tener pasaporte.
				-Tener ahorros en el banco y cuentas activas permanentemente.
				-Tener dinero para sustentar todos los gasto durante su estancia en Canada.
				</p>
				</div>
			</div>
			<div id="conprim">
				<div id="conimage"><img id="iconos" src="imagenes/funcionar2.png" alt="">
				</div>
				<div id="context">
				<h2>Itinerarios</h2>
				<p>Agencia destinos ofrece a todos los clientes la cotizacion de de los tiquetes aereos 
				hacia las ciudades donde desea viajar, ayudandole a complementar el itinerario de viaje.</p>
				</div>
			</div>
			<div id="conprim">
				<div id="conimage"><img id="iconos" src="imagenes/famili.png" alt="">
				</div>
				<div id="context">
				<h2>Visa familiar</h2>
				<p> si desea visa canadiense para su familia realizamos el tramite para cada solicitante de la familia, aprovechando un descuento especial indicandole todos los requisitos para su visa ante la embajada Canadiense.  </p>
				</div>
			</div>
			<div id="conprim">
				<div id="conimage"><img id="iconos" src="imagenes/trofe.png" alt="">
				</div>
				<div id="context">
				<h2>Inmigracion</h2>
				<p>Contamos con el conocimiento adecuado para brindarle asesoramiento frente a tramites de inmigracion, observamos su perfil y le asesoramos frente a cual programa de inmigracion se puede inscribir.</p>
				</div>
			</div>
			<div id="conprim">
				<div id="conimage"><img id="iconos" src="imagenes/economia.png" alt="">
				</div>
				<div id="context">
				<h2>Economico</h2>
				<p>La asesoria y tramite de la visa canadiense por persona es de $150.000.
				para grupos de 3 personas en adelante el valor es de solo $120.000.</p>
				</div>
			</div>
			<div class="contentAsk" style="background: #ffffffb5">
				<h3 class="titleAsk">Preguntas Visa Canada</h3>
    			<?php require __DIR__.'/../asesorias/preguntas/visa-canada.php'; ?>
				
			</div>
			
			


		</section>
			<section  id="seccioncontacto">
				<form action="formato.php" method="POST" accept-charset="utf-8">
					<h2>CONTACTO</h2>
					<input type="text" name="nombre" placeholder="Nombre" required>
					<input type="text" name="correo" placeholder="Correo" required>
					<input type="text" name="telefono" placeholder="Telefono" required>
					<textarea name="mensaje" placeholder="Escribe aqui tu mensaje."></textarea>
					<input type="submit" id="boton" value="ENVIAR">
				</form>
			</section>

		<footer id="pies">
			<div class="">
				<a data-action="close" id="botoncontac" href="#"><h2>Contactenos</h2></a>
				<div id="texuno"><img id="iconitos" src="imagenes/tecnologia.png" alt=""><p id="texto">3204199366</p></div>
				<div class="dos" id="texuno"><img id="iconitos" src="imagenes/whatsapp-1.png" alt=""><p id="texto1">057+3204199366</p></div>
				<a href="https://www.facebook.com/Agencia-Destinos-Tolima-247368372126520/?fref=ts"><div  id="texuno"><img id="iconitos" src="imagenes/facebook.png" alt=""><p id="texto1">Agencia destinos</p></div></a>
			</div>
		</footer>

		
		</div>
	
	<style type="text/css">
		div.contentAsk{
			padding: 10px;

		}

		h3.titleAsk{
			padding: 10px;
			margin-bottom: 15px;
		}

		ul.preguntas{
				list-style: circle ;
			    color: black;
			}
			ul.preguntas li{
				display: inline-block;
				width: 49%;
				font-weight: 700;
				list-style: circle;
				padding: 3px;
			}
			ul.preguntas li a{
				color: black;
				list-style: circle;
				font-style: none;
				text-decoration: none;
			}
	</style>
	
	<script src="particulas.js"></script>
</body>
</html>